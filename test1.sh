#!/bin/bash
# Script to take a series of images.
v4l2-ctl -d /dev/video0 -c exposure_auto=1 -c exposure_absolute=5
fswebcam -d /dev/video0 -r 1280x720 -f 2 -S 5 imga_e5.jpg
v4l2-ctl -d /dev/video0 -c exposure_auto=1 -c exposure_absolute=10
fswebcam -d /dev/video0 -r 1280x720 -f 2 -S 5 imgb_e10.jpg
v4l2-ctl -d /dev/video0 -c exposure_auto=1 -c exposure_absolute=20
fswebcam -d /dev/video0 -r 1280x720 -f 2 -S 5 imgc_e20.jpg
v4l2-ctl -d /dev/video0 -c exposure_auto=1 -c exposure_absolute=39
fswebcam -d /dev/video0 -r 1280x720 --frequency 2 -S 5 imgd_e30.jpg
v4l2-ctl -d /dev/video0 -c exposure_auto=1 -c exposure_absolute=78
fswebcam -d /dev/video0 -r 1280x720 --frequency 2  -S 5 imge_e78.jpg
v4l2-ctl -d /dev/video2 -c exposure_auto=1 -c exposure_absolute=156
fswebcam -d /dev/video0 -r 1280x720 --frequency 2 -S 5 imgf_e156.jpg
v4l2-ctl -d /dev/video0 -c exposure_auto=1 -c exposure_absolute=312
fswebcam -d /dev/video0 -r 1280x720 --frequency 2 -S 5 imgg_e312.jpg
v4l2-ctl -d /dev/video0 -c exposure_auto=1 -c exposure_absolute=625
fswebcam -d /dev/video0 -r 1280x720 --frequency 2 -S 5 imgh_e625.jpg
v4l2-ctl -d /dev/video0 -c exposure_auto=1 -c exposure_absolute=1250
fswebcam -d /dev/video0 -r 1280x720 --frequency 2 -S 5 imgi_e1250.jpg
v4l2-ctl -d /dev/video0 -c exposure_auto=1 -c exposure_absolute=2500
fswebcam -d /dev/video0 -r 1280x720 --frequency 2 -S 5 imgj_e2500.jpg
v4l2-ctl -d /dev/video0 -c exposure_auto=1 -c exposure_absolute=5000
fswebcam -d /dev/video0 -r 1280x720 --frequency 2 -S 5 imgk_e5000.jpg
v4l2-ctl -d /dev/video0 -c exposure_auto=1 -c exposure_absolute=10000
fswebcam -d /dev/video0 -r 1280x720 --frequency 2 -S 5 imgl_e10000.jpg
v4l2-ctl -d /dev/video0 -c exposure_auto=1 -c exposure_absolute=20000
fswebcam -d /dev/video0 -r 1280x720 --frequency 2 -S 5 imgm_e20000.jpg

# Reset Exposure to Auto
v4l2-ctl -d /dev/video0 -c exposure_auto=3 
fswebcam -d /dev/video0 -r 1280x720 --frequency 2 -S 5 imgm_eAuto.jpg


