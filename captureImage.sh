#!/bin/bash
# 
# Shell Script to capture an image from /dev/video0
#

# Reset Exposure to Auto
#v4l2-ctl -d /dev/video0 -c exposure_auto=3
#fswebcam -d /dev/video0 -r 1280x720 --frequency 2 -S 5 imgm_eAuto.jpg
fswebcam -d /dev/video0 -S 2 $1.jpg

