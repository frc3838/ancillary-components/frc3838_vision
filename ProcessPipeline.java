// Class ProcessPipeline
// Written by Kurt M. Sanger
// Created 3/2/2020
// Takes ArrayList<MatOfPoints> from gripPipeline and looks for FRC 2020 Power Port Inner Goal Target.


import java.util.ArrayList;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.imgproc.Imgproc;


public class ProcessPipeline {
	//public static final double TARGET_HEIGHT = 17;  // Target Height (in)
	//public static final double TARGET_WIDTH = 39.25;  // Target Width (in)
	//public static final double TARGET_ASPECT_RATIO = (TARGET_HEIGHT/TARGET_WIDTH);
	//public static final double MIN_AREA = 10.0;  // Minimum area of contour to check
	//public static final double MAX_ASPECT_RATIO_DIFFERENCE = 0.20;

	//private static Target myTarget;
	
	private static final boolean FLGDEBUG = false;
	
	public ProcessPipeline() {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		
		//myTarget = new Target();
	}
	
	
	public static boolean updateTarget(ArrayList<MatOfPoint> contourArray, Target target) {
		boolean flgNewTarget = false;
		if( contourArray.size() > 0) {
			// We have contours to check.
			double maxArea = 0;
			double bestAspectRatioDifference = 1.0;
			int maxAreaIndex = -1;
			Rect rect = new Rect(0,0,0,0);
			Rect bestRect = new Rect( 0,0,0,0);
			int bestRectIndex = -1;
			
			for (int i=0; i<contourArray.size(); i++) {
				Mat contour = contourArray.get(i);
				double contourArea = Imgproc.contourArea(contour);
				if( FLGDEBUG) {
					System.out.println("Area for contour[" + 
						Integer.toString(i) +"] is " + Double.toString(contourArea));
				}
				if (contourArea > target.MIN_AREA) {
					if( FLGDEBUG) System.out.println("We have a contour to test");
					if (contourArea > maxArea) {
						// We have a new maximum area.
						if (FLGDEBUG) System.out.println("New Maximum Area");
						maxArea = contourArea;
						maxAreaIndex = i;
					}
					
					rect = Imgproc.boundingRect(contour);
					double aspectRatio = ((double)rect.height) / ((double)rect.width);
					if (FLGDEBUG) System.out.println("Rectangle Aspect Ratio " + Double.toString(aspectRatio));
					if (FLGDEBUG) System.out.println("Target Aspect Ratio = " + Double.toString(target.TARGET_ASPECT_RATIO));
					
					double absAspectRatioDifference = Math.abs(target.TARGET_ASPECT_RATIO - aspectRatio);
					if (FLGDEBUG) System.out.println("Absolut Aspect Ratio Difference = " + absAspectRatioDifference);
					if( absAspectRatioDifference < bestAspectRatioDifference ) {
						// We have a better match to target.
						if (FLGDEBUG) System.out.println("New Better Aspect Ratio");
						if(absAspectRatioDifference <= target.MAX_ASPECT_RATIO_DIFFERENCE) {
							// We have a valid target with low aspect ratio difference.
							if (FLGDEBUG) System.out.println("New Target");
							bestAspectRatioDifference = absAspectRatioDifference;
							bestRectIndex = i;
							bestRect = rect.clone();
							flgNewTarget = true;
						}
					}
				} 
				// We have no more contours to check.  No possible target.
				if( flgNewTarget) {
					// Update Target Data
					target.updateTarget(bestRect);
					if (FLGDEBUG) System.out.println("Updating Target");
				} else {
					target.resetTarget();
					if (FLGDEBUG) System.out.println("Reseting Target");
				}
			}
		} // End 
		return(flgNewTarget);
	}
	
}
