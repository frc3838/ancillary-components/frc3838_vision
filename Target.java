import org.opencv.core.Core;
//import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;

public class Target {
	private static boolean flgTargetFound;
	private static Point targetXY;  // Note goal is at top of rectangle found.
	private static Rect targetRect; // Rectangle that fits Target.
	
	public static final double TARGET_HEIGHT = 17;  // Target Height (in)
	public static final double TARGET_WIDTH = 39.25;  // Target Width (in)
	public static final double TARGET_ASPECT_RATIO = (TARGET_HEIGHT/TARGET_WIDTH);
	public static final double MIN_TARGET_ASPECT_RATIO = 0.90*TARGET_ASPECT_RATIO;
	public static final double MAX_TARGET_ASPECT_RATIO = 1.1*TARGET_ASPECT_RATIO;
	public static final double MIN_AREA = 20.0;  // Minimum area of contour to check
	public static final double MAX_ASPECT_RATIO_DIFFERENCE = 0.20;
	
	public Target() {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		flgTargetFound = false;
		targetXY = new Point(0.0,0.0);
		targetRect = new Rect(0,0,0,0);
	}
	
	public void setTargetFound() {
		flgTargetFound = true;
	}
	public void resetTargetFound() {
		flgTargetFound = false;
	}
	public boolean getTargetFound() {
		return flgTargetFound;
	}

	public Point getCentroid() {
		Point c = new Point(-1.0,-1.0);
		if (flgTargetFound) {
			c.x = ((double) targetRect.width) / 2.0;
			c.x += ((double) targetRect.x);
			c.y = ((double) targetRect.height) / 2.0;
			c.y += ((double) targetRect.y);
		} 
		return c;
	}
	
	/** Functions Derived from Rectangle Fit of Target
	 * 
	 * @return
	 */
	public double getCentroidX() {
		return getCentroid().x;
	}
	
	public double getCentroidY() {
		return getCentroid().y;
	}
	
	public double getWidth() {
		if(flgTargetFound) {
			return (double) targetRect.width;
		} else {
			return -1.0;
		}
	}
	
	public double getHeight() {
		if(flgTargetFound) {
			return (double) targetRect.height;
		}
		return -1.0;
	}
		
	
	public double getAspectRatio() {
		if(flgTargetFound) {
			if( targetRect.width != 0.0) {
				return ((double) targetRect.height) / ((double) targetRect.width);
			} else {
				return 1000000.0;  // A Large Number
			}
		}
		return -1.0;
	}
	
	/** Functions Regarding Actual Goal 
	 * 
	 * @param xy
	 */
	private void setTargetXY(Point xy)  {
		targetXY = xy.clone();
	}
	
	private void setTargetX(double x) {
		targetXY.x = x;
	}
	
	private void setTargetY(double y) {
		targetXY.y = y;
	}
	
	private void setTargetXY(double x, double y) {
		targetXY.x = x;
		targetXY.y = y;
	}
	
	public Point getTargetXY() {
		if( flgTargetFound) {
			return(targetXY);
		} else {
			Point xy = new Point(-1.0,-1.0);
			return xy;
		}
	}
	
	public double getTargetX() {
		if( flgTargetFound) {
			return targetXY.x;
		} else {
			return -1.0;
		}
	}
	
	public double getTargetY() {
		if( flgTargetFound) {
			return targetXY.y;
		} else {
			return -1.0;
		}
	}
	
	
	private void resetTargetRect() {
		Rect nullRect = new Rect(0,0,0,0);
		targetRect = nullRect.clone();
	}
	
	private void resetTargetXY() {
		setTargetXY(-1.0,-1.0);
	}
	
	
	/** Call if Target Not Found
	 * 
	 */
	public void resetTarget() {
		resetTargetFound();
		resetTargetXY();
		resetTargetRect();
	}
	
	/** Call if Target Found
	 * 
	 * @param r
	 */
	public void updateTarget(Rect r) {
		// Clone Rectangle fit to target
		targetRect = r.clone();
		double x = getCentroid().x;
		double y = r.y;
		setTargetXY(x,y);
		setTargetFound();
	}
	
	
	
}
