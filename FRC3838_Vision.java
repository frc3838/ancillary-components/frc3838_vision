import java.util.ArrayList;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;
//import org.opencv.videoio.Videoio;

import edu.wpi.cscore.CvSink;
import edu.wpi.cscore.CvSource;
//import edu.wpi.cscore.MjpegServer;
import edu.wpi.cscore.UsbCamera;
//import edu.wpi.cscore.VideoMode;
import edu.wpi.first.wpilibj.CameraServer;
//import edu.wpi.cscore.VideoSource;

public class FRC3838_Vision {

	static final int CAP_WIDTH=1280;
	static final int CAP_HEIGHT=720;
	static final int SM_CAP_WIDTH=320;
	static final int SM_CAP_HEIGHT=240;
	static final int DISPLAY_WIDTH=320;
	static final int DISPLAY_HEIGHT=240;
	static final int MJPEG_DISPLAY_SERVER_PORT=1186;
	static final int FRAME_PERIOD_NSEC = 2000;
	static final int CAP_AUTO_EXPOSURE=3;
	static final int CAP_EXPOSURE=10;
	static final int CAP_EXPOSURE_ABS=78;
	static final int CAP_EXPOSURE_ABS_PERCENT=20;  // Uses percentage.
	static final int CAP_WHITE_BALANCE_MANUAL=4500;
	static final int CAP_FPS=15;
	
	static final Size SMALL_IMG_SIZE = new Size(SM_CAP_WIDTH,SM_CAP_HEIGHT);
	static final Size LARGE_IMG_SIZE = new Size(CAP_WIDTH,CAP_HEIGHT);
	
	static final Point FRONTLINEAPT1 = new Point(100,1);
	static final Point FRONTLINEAPT2 = new Point(100,239);
	static final Point FRONTLINEBPT1 = new Point(220,1);
	static final Point FRONTLINEBPT2 = new Point(220,239);
	
	static final Point REARLINEAPT1 = new Point(75,1);
	static final Point REARLINEAPT2 = new Point(100,239);
	static final Point REARLINEBPT1 = new Point(245,1);
	static final Point REARLINEBPT2 = new Point(220,239);
	
	static final Scalar WHITE = new Scalar(255,255,255);
	static final Scalar GREEN = new Scalar(0,255,0);
	static final Scalar RED = new Scalar(0,0,255);
	static final Scalar YELLOW = new Scalar(0,255,255);
	
	
	private static VideoNetworkTable videoNT;
	// initialize cameras
    private static boolean flgFrontCamera = false;
    private static boolean flgRearCamera = false;
    private static boolean flgFront = true;
    private static boolean flgTarget = false;
    private static boolean flgRear = false;
    
    //private static final int CAMERAFRONT = 0;  // d
    //private static final int CAMERAREAR = 2;   // d
    private static final int FRONTCAMERA = 0;
    private static final int REARCAMERA = 2;
    
    static VideoCapture cameraFront;  // d
    static VideoCapture cameraRear;   // d
    
    static CameraServer cameraServer;  //n
    static UsbCamera frontCamera; //n
    static UsbCamera rearCamera;  //n
    
    static CvSink inputStream; //n
    static CvSource  outputStream;   //n

    static final String strOutputStream = "streamOut";
    

    private static boolean flgDebug = false;
	
    // duplicated here so that they are constants for switch statement.
	public final static int FRONT = 0;
	public final static int TARGET = 1;
	public final static int REAR = 2;

	
    // Create Image Source to send frames/mats through to SmartDashboard.
	//private static CvSource imageSource;
	//private static MjpegServer cvStream;
	private static Mat frame;
	private static Mat largeFrame;
	
	
	/** Vision Processing Defines.
	 * 
	private static GripPipeline pipe = new GripPipeline();
	private static Target target = new Target();
	private static ArrayList<MatOfPoint> contours = new ArrayList<MatOfPoint>(); 
	private static ProcessPipeline process = new ProcessPipeline();
	*/
	private static GripPipeline pipe;
	private static Target target;
	private static ArrayList<MatOfPoint> contours; 
	private static ProcessPipeline process;

	
	public static void main(String[] args) {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		
		flgDebug = false;
		if( args.length > 0) {
			if( args[0].startsWith("d")) {
				flgDebug = true;
			} 
		}
		
		videoNT = new VideoNetworkTable();
		videoNT.init();
	    // Delay for network tables to open

		// Cameras, Input Sinks, Output Sources
		cameraServer = CameraServer.getInstance();
		frontCamera = cameraServer.startAutomaticCapture("FrontCamera",FRONTCAMERA);
		rearCamera = cameraServer.startAutomaticCapture("RearCamera",REARCAMERA);
		inputStream = CameraServer.getInstance().getVideo("FrontCamera");
		inputStream.setEnabled(true);
		outputStream = CameraServer.getInstance().putVideo(strOutputStream, SM_CAP_WIDTH, SM_CAP_HEIGHT);
	    
	    frame = new Mat();  // Matrix used for processing.
	    largeFrame = new Mat();
	    
	    // Vision Processing

		pipe = new GripPipeline();
		target = new Target();
		contours = new ArrayList<MatOfPoint>(); 
		process = new ProcessPipeline();
	    
	    try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    if (frontCamera.isConnected()) {
	    	videoNT.setFrontCameraFound();
	    	flgFrontCamera=true;
	    	setFrontCameraParams();
	    } else {
	    	videoNT.setFrontCameraNotFound();
	    	if(flgDebug) {
	    		System.out.println("Front Camera Not Opened.");
	    	}
	    }
	    
	    if (rearCamera.isConnected()) {
	    	videoNT.setRearCameraFound();
	    	flgRearCamera=true;
	    	setRearCameraParams();
	    } else {
	    	videoNT.setRearCameraNotFound();
	    	if(flgDebug) {
	    		System.out.println("Rear Camera Not Opened.");
	    	}
	    }
	    
	    run();
	}
	
	private static void run() {
		// initialize loop.
		int lastCamera = FRONT;
		int currentCamera = FRONT;
		
		
		
		
		while(true) {
			// update video mode
			currentCamera = videoNT.updateVideoMode();  // get Video Mode from Network Table.
			if(flgDebug) System.out.println("Current Camera " + currentCamera);
			
			// if change then change camera.
			if(currentCamera != lastCamera) {
				if(flgDebug) System.out.println("Changing Camera.");
				// Change Camera Mode
				switch (currentCamera) {
				case FRONT :
					setupFrontCamera();
					lastCamera = FRONT;
					break;
				case TARGET :
					setupTargetCamera();
					lastCamera = TARGET;
					break;					
				case REAR :
					setupRearCamera();  // Rear Camera Settings do not change.
					lastCamera = REAR;
					break;
				default :
					setFrontCameraParams();
					lastCamera = FRONT;
					break;
				}				
			}
			// grab read frame & Process Frame & Stream Frame
			if(flgDebug) System.out.println("Processing Camera Frame.");

			switch (currentCamera) {
			case FRONT :
				if(flgDebug) System.out.println("Calling Display Front.");
				displayFront();
				// process and display front camera
				break;
			case TARGET :
				if(flgDebug) System.out.println("Calling Process Target.");
				// process Target and display.
				processTarget();
				break;
			case REAR :
				if(flgDebug) System.out.println("Calling Display Rear.");
				displayRear();
				// process and display rear camera
				break;
			default :
				if(flgDebug) System.out.println("Defaulting to Display Front.");
				// process and display front camera
				displayFront();
				break;					
			}
			if(flgDebug) System.out.println("Outputting Frame.");
			
			// Display Camera Frame.
			if(flgDebug) System.out.println("Frame Channels: " + frame.channels());
			if(flgDebug) System.out.println("Frame Columns: " + frame.cols());
			if(flgDebug) System.out.println("Frame Rows: " + frame.rows());

			outputStream.putFrame(frame);
		}
	}
	
	private static void processTarget() {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		// display Front for now.
		//while(!cameraFront.grab()) {
		//	try {
		//		Thread.sleep(17);
		//	} catch (InterruptedException e) {
		//		// TODO Auto-generated catch block
		//		e.printStackTrace();
		//	}
		//}
		long result = inputStream.grabFrame(largeFrame);
		if(result == 0) {
			// we have an error.
			if(flgDebug) System.out.println("grabFrame ERROR: " + inputStream.getError());
			largeFrame = Mat.zeros(LARGE_IMG_SIZE, CvType.CV_8UC3 );
		}

		pipe.process(largeFrame);
		contours = pipe.findContoursOutput();
		boolean flgTargetFound = ProcessPipeline.updateTarget(contours, target);

		if(flgTargetFound) {
			if(flgDebug) System.out.println("TARGET FOUND");
			updateNT_TargetFound();
			Point pt11 = new Point(target.getCentroidX()-target.getWidth()/2,
									target.getCentroidY());
			Point pt12 = new Point(target.getCentroidX()+target.getWidth()/2,
									target.getCentroidY());
			if(pt11.x < 1.0) {
				pt11.x=1.0;
			}
			if(pt12.x > (CAP_WIDTH-1) ) {
				pt12.x = CAP_WIDTH-1;
			}
			
			Point pt21 = new Point(target.getCentroidX(),
					target.getCentroidY()+target.getHeight());
			Point pt22 = new Point(target.getCentroidX(),
					target.getCentroidY()-target.getHeight());
			
			if(pt21.y>(CAP_HEIGHT-1)) {
				pt21.y = (CAP_HEIGHT-1);
			}
			if(pt22.y<1.0) {
				pt22.y=1.0;
			}
			if(flgDebug) {
				System.out.println("pt11 [" + pt11.x + "," + pt11.y + "]");
				System.out.println("pt12 [" + pt12.x + "," + pt12.y + "]");
				System.out.println("pt21 [" + pt21.x + "," + pt21.y + "]");
				System.out.println("pt22 [" + pt22.x + "," + pt22.y + "]");
			}
			Imgproc.line(largeFrame, pt11, pt12, GREEN, 12);
			Imgproc.line(largeFrame, pt21, pt22, GREEN, 12);
		} else {
			if(flgDebug) System.out.println("TARGET NOT FOUND");
			updateNT_TargetNotFound();
		}
		//cameraFront.retrieve(largeFrame);
		Imgproc.resize(largeFrame, frame, SMALL_IMG_SIZE);
	}
	
	private static void updateNT_TargetFound() {
		videoNT.setTargetFound();
		videoNT.putTargetCentroidX(target.getCentroidX());
		videoNT.putTargetCentroidY(target.getCentroidY());
		videoNT.putTargetHeight(target.getHeight());
	}
	private static void updateNT_TargetNotFound() {
		videoNT.resetTargetFound();
	}
	
	
	private static void displayFront() {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

		//while(!cameraFront.grab()) {
		//	try {
		//		Thread.sleep(17);
		//	} catch (InterruptedException e) {
		//		// TODO Auto-generated catch block
		//		e.printStackTrace();
		//	}
		//}
		//cameraFront.retrieve(frame);
		long result = inputStream.grabFrame(frame);
		if(result == 0) {
			// we have an error.
			if(flgDebug) System.out.println("grabFrame ERROR: " + inputStream.getError());
			frame = Mat.zeros(SMALL_IMG_SIZE, CvType.CV_8UC3 );
		}
		
		Imgproc.line(frame, FRONTLINEAPT1, FRONTLINEAPT2, YELLOW);
		Imgproc.line(frame, FRONTLINEBPT1, FRONTLINEBPT2, YELLOW);
		
	}
	
	private static void displayRear() {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

		//while(!cameraRear.grab()) {
		//	try {
		//		Thread.sleep(17);
		//	} catch (InterruptedException e) {
		//		// TODO Auto-generated catch block
		//		e.printStackTrace();
		//	}
		//}
		long result = inputStream.grabFrame(frame);
		if(result == 0) {
			// we have an error.
			if(flgDebug) System.out.println("grabFrame ERROR: " + inputStream.getError());
			frame = Mat.zeros(SMALL_IMG_SIZE, CvType.CV_8UC3 );
		}
		//cameraRear.retrieve(frame);
		Imgproc.line(frame, REARLINEAPT1, REARLINEAPT2, YELLOW);
		Imgproc.line(frame, REARLINEBPT1, REARLINEBPT2, YELLOW);
	}
	
	private static void setupFrontCamera() {
		setFrontCameraParams();
		inputStream = CameraServer.getInstance().getVideo("FrontCamera");
		inputStream.setEnabled(true);
	}
	
	private static void setupTargetCamera() {
		setTargetCameraParams();
		inputStream = CameraServer.getInstance().getVideo("FrontCamera");
		inputStream.setEnabled(true);
	}
	
	private static void setupRearCamera() {
		setRearCameraParams();
		inputStream = CameraServer.getInstance().getVideo("RearCamera");
		inputStream.setEnabled(true);
	}
	
	private static void setFrontCameraParams() {
		frontCamera.setResolution(SM_CAP_WIDTH, SM_CAP_HEIGHT);
		frontCamera.setFPS(CAP_FPS);
		frontCamera.setExposureAuto();
		frontCamera.setWhiteBalanceAuto();
    	//cameraFront.set(Videoio.CAP_PROP_FRAME_HEIGHT, SM_CAP_HEIGHT);
    	///cameraFront.set(Videoio.CAP_PROP_FRAME_WIDTH,SM_CAP_WIDTH);
    	//cameraFront.set(Videoio.CAP_PROP_FPS, CAP_FPS);
    	//cameraFront.set(Videoio.CAP_PROP_AUTO_EXPOSURE, CAP_AUTO_EXPOSURE);		
    	if (flgDebug) {
    		displayUsbCameraParams(frontCamera);
    	}
	}
	
	private static void setTargetCameraParams() {
		frontCamera.setResolution(CAP_WIDTH, CAP_HEIGHT);
		frontCamera.setFPS(CAP_FPS);
		frontCamera.setExposureManual(CAP_EXPOSURE_ABS_PERCENT);

		frontCamera.setWhiteBalanceManual(CAP_WHITE_BALANCE_MANUAL);
    	if (flgDebug) {
    		displayUsbCameraParams(frontCamera);
    	}
	}
	
	private static void setRearCameraParams() {
		rearCamera.setResolution(SM_CAP_WIDTH, SM_CAP_HEIGHT);
		rearCamera.setFPS(CAP_FPS);
		rearCamera.setExposureAuto();
		rearCamera.setWhiteBalanceAuto();
		//cameraRear.set(Videoio.CAP_PROP_FRAME_HEIGHT, SM_CAP_HEIGHT);
    	//cameraRear.set(Videoio.CAP_PROP_FRAME_WIDTH,SM_CAP_WIDTH);
    	//cameraRear.set(Videoio.CAP_PROP_FPS, CAP_FPS);
    	//cameraRear.set(Videoio.CAP_PROP_AUTO_EXPOSURE, CAP_AUTO_EXPOSURE);
    	if (flgDebug) {
    		displayUsbCameraParams(rearCamera);
    	}
	}
	
//	private static void displayCameraParams(VideoCapture cam) {  // Obsolete
//		for (int i = 0; i<21; i++) {
//			System.out.print("Camera Parameter " + i + ":");
//			System.out.println(cam.get(i));
//		}
//	}
	
	private static void displayUsbCameraParams(UsbCamera cam) {
		System.out.println("Width " + cam.getProperty("width"));
		System.out.println("Height " + cam.getProperty("height"));
		System.out.println("Saturation " + cam.getProperty("saturation"));
		System.out.println("brightness " + cam.getProperty("brightness"));
		System.out.println("exposure_absolute " + cam.getProperty("exposure_absolute"));
		System.out.println("exposure_auto_priority " + cam.getProperty("exposure_auto_priority"));
	}
}
