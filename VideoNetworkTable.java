//import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.networktables.NetworkTable;

public class VideoNetworkTable {
	NetworkTable videoNT;
	
	private final int TEAM=3838;
	private final String ROBORIOADDRESS = "roborio-3838-frc.local";
	private final String TABLENAME = "FRC3838_Vision";
	

	public final int FRONT = 0;
	public final int TARGET = 1;
	public final int REAR = 2;
	private int currentVideoMode;
	
	private static boolean flgNetworkTableFound = false;
	private final boolean FLGDEBUG = false;
	
	
	public void init() {
		// Initialize NetworkTable
		NetworkTable.setClientMode();
		NetworkTable.setTeam(TEAM);
		NetworkTable.setIPAddress(ROBORIOADDRESS); // ip of roborio
		NetworkTable.initialize();
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  // Wait for table
		if (FLGDEBUG) System.out.println("Checking on " + TABLENAME);
		
		videoNT = NetworkTable.getTable(TABLENAME); // what table data is put in
		videoNT.putBoolean("Pi Found", true);
		
		if( videoNT.containsKey("Set Video Mode")) {
			updateVideoMode();
		} else {
			currentVideoMode = FRONT;
			videoNT.putString("Video Mode", "Front");
			videoNT.putNumber("Set Video Mode", FRONT);
			videoNT.putNumber("VM", 0);
			updateVideoMode();
			resetTargetFound();
		}
		if (FLGDEBUG) System.out.println("Video Network Table Initiali"
				+ "zed.");
	}
	
	public int updateVideoMode() {
		if (FLGDEBUG) System.out.println("Update Video Mode Called");
		if (videoNT.isConnected() ) {
			if (FLGDEBUG) System.out.println("Network Table Found.");
			flgNetworkTableFound = true;
			currentVideoMode = (int) videoNT.getNumber("VM", -1.0);
			switch (currentVideoMode) {
			case FRONT :
				videoNT.putString("Video Mode","Front");
				break;
			case TARGET :
				videoNT.putString("Video Mode", "Target");
				break;
			case REAR :
				videoNT.putString("Video Mode", "Rear");
				break;
			default :
				videoNT.putString("Video Mode", "ERROR Defaulting to Front");
				currentVideoMode = 0;
				break;
			}
		} else {
			currentVideoMode = 0;
			flgNetworkTableFound = false;
			if (FLGDEBUG) System.out.println("ERROR Network Table Not Found");
		}
		return (currentVideoMode);			
	}
	
	public void setFrontCameraFound() {
		if(flgNetworkTableFound) {
			videoNT.putBoolean("Front Camera Found", true);
		}
	}
	public void setFrontCameraNotFound() {
		if(flgNetworkTableFound) {
			videoNT.putBoolean("Front Camera Found", false);
		}
	}
	
	public void setRearCameraFound() {
		if(flgNetworkTableFound) {
			videoNT.putBoolean("Rear Camera Found", true);
		}
	}
	public void setRearCameraNotFound() {
		if(flgNetworkTableFound) {
			videoNT.putBoolean("Rear Camera Found", false);
		}
	}
	
	public void setTargetFound() {
		if(flgNetworkTableFound) {
			videoNT.putBoolean("Target Found", true);
		}
	}
	public void resetTargetFound() {
		if(flgNetworkTableFound) {
			videoNT.putBoolean("Target Found", false);
			putTargetCentroidX(-1.0);
			putTargetCentroidY(-1.0);
			putTargetHeight(-1.0);
		}		
	}

	public void putTargetHeight(double h) {
		if(flgNetworkTableFound) {
			videoNT.putNumber("Target Height",h);
			// Compute distance based on target height.
		}			
	}
	
	public void putTargetCentroidX(double x) {
		if(flgNetworkTableFound) {
			videoNT.putNumber("Target Centroid X",x);
			// Compute Rotational Angle based on Centroid X
		}			
	}
	
	public void putTargetCentroidY(double y) {
		if(flgNetworkTableFound) {
			videoNT.putNumber("Target Centroid Y",y);
			// Compute Target Height based on Centroid Y
		}			
	}
}
